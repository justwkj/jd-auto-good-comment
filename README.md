## 京东自动好评,追加图片
网上找的京东自动好评的的代码,自己简单的修改一下

## 自动好评使用
1. 打开页面待评论的页面 [https://club.jd.com/myJdcomments/myJdcomments.action?sort=0](https://club.jd.com/myJdcomments/myJdcomments.action?sort=0) 或者 [http://club.jd.com/myJdcomments/myJdcomments.action?sort=0](http://club.jd.com/myJdcomments/myJdcomments.action?sort=0)
2. 复制goods.js的代码在浏览器console中执行
    
## 图片追加评论使用
1. 打开页面待评论的页面 [https://club.jd.com/myJdcomments/myJdcomments.action?sort=1](https://club.jd.com/myJdcomments/myJdcomments.action?sort=1) 或者 [http://club.jd.com/myJdcomments/myJdcomments.action?sort=1](http://club.jd.com/myJdcomments/myJdcomments.action?sort=1)
2. 复制appendImg.js的代码在浏览器console中执行
